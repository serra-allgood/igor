# Copyright 2019-2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Igor.Responder.HelpTest do
  use ExUnit.Case

  test "help" do
    requests = [
      {
        %Polyjuice.Client.Endpoint.PostRoomsReceipt{
          event_id: "$event_id",
          receipt_type: "m.read",
          room: "!room_id"
        },
        :ok
      },
      {
        %Polyjuice.Client.Endpoint.PutRoomsSend{
          room: "!room_id",
          txn_id: "txn_id",
          event_type: "m.room.message",
          message: %{
            "body" =>
              "igor help [command...] - display help\nigor ping [response] - respond with a pong\nigor uptime - show how long the bot has been running",
            "msgtype" => "m.notice"
          }
        },
        {:ok, "$event_id"}
      }
    ]

    messages = %{
      "!room_id" => [
        %{
          "sender" => "@alice:example.com",
          "type" => "m.room.message",
          "content" => %{
            "msgtype" => "m.text",
            "body" => "igor: help"
          },
          "room_id" => "!room_id",
          "event_id" => "$event_id"
        }
      ]
    }

    {:ok, pid} =
      Igor.start_link(
        make_client: fn handler, _, _ ->
          DummyClient.create("@igor:example.com", handler, requests, messages)
        end,
        bot_name: "igor",
        mxid: "@igor:example.com",
        responders: [
          Igor.Responder.Help,
          Igor.Responder.Ping,
          {Igor.Responder.Uptime, :create, []}
        ]
      )

    client = Igor.get_client(pid)

    {:ok, _} = DummyClient.await(client)
  end

  test "help for command" do
    requests = [
      {
        %Polyjuice.Client.Endpoint.PostRoomsReceipt{
          event_id: "$event_id",
          receipt_type: "m.read",
          room: "!room_id"
        },
        :ok
      },
      {
        %Polyjuice.Client.Endpoint.PutRoomsSend{
          room: "!room_id",
          txn_id: "txn_id",
          event_type: "m.room.message",
          message: %{
            "body" =>
              "igor help [command...] - display help\n\nIf `command` is given, then display help for the given command.  Otherwise, display a list of commands that the bot understands.",
            "format" => "org.matrix.custom.html",
            "formatted_body" =>
              "igor help [command...] - display help<br /><br />If <code>command</code> is given, then display help for the given command.  Otherwise, display a list of commands that the bot understands.",
            "msgtype" => "m.notice"
          }
        },
        {:ok, "$event_id"}
      }
    ]

    messages = %{
      "!room_id" => [
        %{
          "sender" => "@alice:example.com",
          "type" => "m.room.message",
          "content" => %{
            "msgtype" => "m.text",
            "body" => "igor: help help"
          },
          "room_id" => "!room_id",
          "event_id" => "$event_id"
        }
      ]
    }

    {:ok, pid} =
      Igor.start_link(
        make_client: fn handler, _, _ ->
          DummyClient.create("@igor:example.com", handler, requests, messages)
        end,
        bot_name: "igor",
        mxid: "@igor:example.com",
        responders: [
          Igor.Responder.Help
        ]
      )

    client = Igor.get_client(pid)

    {:ok, _} = DummyClient.await(client)
  end
end

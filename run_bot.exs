require Logger

Logger.warn "Using this script is deprecated.  Please use `mix igor.run` instead."

{:ok, pid} = Igor.start_link(Application.get_all_env(:igor))
ref = Process.monitor(pid)
receive do
  {:DOWN, ^ref, :process, _object, reason} -> reason
end

# Igor

Igor allows you to write bots for [Matrix](https://matrix.org/).

## Installation

The package can be installed by adding `igor` to your list of dependencies in
`mix.exs`:

```elixir
def deps do
  [
    {:igor, "~> 0.2.1"}
  ]
end
```

Igor depends on
[html5ever_elixir](https://github.com/hansihe/html5ever_elixir), which in turn
depends on the html5ever Rust library, so to compile Igor, you will need to
[install Rust](https://www.rust-lang.org/tools/install).

Note that html5ever_elixir 0.7.0 [does not work with OTP
22](https://github.com/hansihe/html5ever_elixir/issues/13), but
html5ever_elixir 0.8.0 does not compile with older versions of Rust (notably,
the version of Rust shipped with Debian Buster), so there isn't a single
version of html5ever_elixir that will work universally. Currently, `mix.lock`
specifies version 0.7.0, but if you need/want to use version 0.8.0, run `mix
deps.unlock html5ever rustler` and then `mix deps.get`.

## Running the bot

### Standalone

To run Igor on its own, create a new Elixir project that depends on it and on
any plugins that you want to use, and run `mix deps.get` to fetch the
dependencies.  Then create a `config/config.exs` file (see
`config/config_sample.exs` for an example), and call `mix igor.run` to start
the bot.

### As part of an application

Igor can also be started from within another Elixir application by calling

```elixir
Igor.start_link(opts)
```

where opts is a keyword list as described in the [configuring](#configuring)
section below.  Or it can be started by a supervisor:

```elixir
children = [
  {Igor, opts}
]
Supervisor.start_link(children, strategy: :one_for_one)
```

## Configuring

Igor takes the following configuration parameters:

- `start_client:` (optional) a function that starts a client process and
  returns `{:ok, pid, client}`, where `client` is something that implements
  `Polyjuice.Client.API`.  The function will be given three arguments: a
  `Polyjuice.Client.Handler`, a `Polyjuice.Client.Storage`, and a sync filter.
- `make_client:` (optional) a function that returns something that implements
  `Polyjuice.Client.API`.  The function will be given three arguments: a
  `Polyjuice.Client.Handler`, a `Polyjuice.Client.Storage`, and a sync filter.
- `client:` (optional) a `Polyjuice.Client.API`.  The `start_client` option
  takes precedence over `make_client`, which takes precedence over `client`.
  If none of these options is specified, Igor will create a new
  `Polyjuice.Client` using the other options given.
- `homeserver_url:` (required if `start_client`, `make_client`, and `client`
  are not set) the base URL of the homeserver
- `access_token:` (optional) the access token for the bot user to authenticate
  with the homeserver
- `device_id:` (optional) the device ID for the bot's session (currenty unused)
- `bot_name:` (optional) the bot's name, for use with responding to commands.
  Default: `igor`
- `mxid:` (required) the bot's Matrix user ID
- `aka:` (optional) list other names that the bot will respond to for commands.
  Default: `[]`
- `command_prefixes:` (optional) list prefixes that can be used for triggering
  commands.  Default: `[]`
- `responders:` (optional) list of [responders](#responders) to use.  Default:
  `[]`
- `storage:` (optional, recommended) persistent storage to use.  The default
  value is not suitable for production use, since it does not persist data
  across restarts.  Default: `{Igor.Storage, :ets, []}`
- `accept_invites:` (optional) whether or not to accept invites.  If `true`,
  accept all invites.  If `false`, reject all invites.  If a list of strings,
  accept invites from any of the users IDs listed, and reject invites from
  others.  Default: `true`

## Responders

Igor uses responders to respond to Matrix events, and comes with some [sample
responders](lib/igor/responder/).  For information on writing your own
responder, see the documentation for `Igor.Responder`.

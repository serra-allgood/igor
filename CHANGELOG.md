0.2.1
=====

Features:

- Allow specifying additional options for Polyjuice Client.
- Allow using newer version of html5ever for compatibility with newer OTP
  versions.

0.2.0
=====

Breaking changes:

- Uses Polyjuice Client v0.3.0, which includes breaking changes from the
  previous version.  If you are not creating your own client, this change
  should not affect you.
  - new configuration options have been added for creating clients

Features:

- Improved spec compliance.
- Add configuration to accept or reject invites.

0.1.0
=====

Initial release.

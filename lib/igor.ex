# Copyright 2019-2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Igor do
  @moduledoc """
  Bot framework for Matrix.

  ## Running the bot

  ### Standalone

  To run Igor on its own, create a new Elixir project that depends on it and on
  any plugins that you want to use, and run `mix deps.get` to fetch the
  dependencies.  Then create a `config/config.exs` file, and call `mix
  igor.run` to start the bot.

  ### As part of an application

  Igor can also be started from within another Elixir application by calling

      Igor.start_link(opts)

  where opts is a keyword list as described in the [configuring](#configuring)
  section below.  Or it can be started by a supervisor:

      children = [
        {Igor, opts}
      ]
      Supervisor.start_link(children, strategy: :one_for_one)

  ## Configuring

  Igor takes the following configuration parameters:

  - `start_client:` (optional) a function that starts a client process and
    returns `{:ok, pid, client}`, where `client` is something that implements
    `Polyjuice.Client.API`.  The function will be given three arguments: a
    `Polyjuice.Client.Handler`, a `Polyjuice.Client.Storage`, and a sync filter.
  - `make_client:` (optional) a function that returns something that implements
    `Polyjuice.Client.API`.  The function will be given three arguments: a
    `Polyjuice.Client.Handler`, a `Polyjuice.Client.Storage`, and a sync filter.
  - `client:` (optional) a `Polyjuice.Client.API`.  The `start_client` option
    takes precedence over `make_client`, which takes precedence over `client`.
    If none of these options is specified, Igor will create a new
    `Polyjuice.Client` using the other options given.
  - `homeserver_url:` (required if `start_client`, `make_client`, and `client`
    are not set) the base URL of the homeserver
  - `access_token:` (optional) the access token for the bot user to authenticate
    with the homeserver
  - `device_id:` (optional) the device ID for the bot's session (currenty unused)
  - `bot_name:` (optional) the bot's name, for use with responding to commands.
    Default: `igor`
  - `mxid:` (optional, deprecated) the bot's Matrix user ID.  Defaults to the user
    ID associated with the client object.
  - `aka:` (optional) list other names that the bot will respond to for commands.
    Default: `[]`
  - `command_prefixes:` (optional) list prefixes that can be used for triggering
    commands.  Default: `[]`
  - `responders:` (optional) list of [responders](#responders) to use.  Default:
    `[]`
  - `storage:` (optional, recommended) persistent storage to use.  The default
    value is not suitable for production use, since it does not persist data
    across restarts.  Default: `{Igor.Storage, :ets, []}`
  - `accept_invites:` (optional) whether or not to accept invites.  If `true`,
    accept all invites.  If `false`, reject all invites.  If a list of strings,
    accept invites from any of the users IDs listed, and reject invites from
    others.  Default: `true`
  - `client_opts:` (optional) Any additional options to pass when creating the
    `Polyjuice.Client`.  This option only has effect if `start_client`,
    `make_client`, and `client` are *not* specified.

  ## Responders

  Igor uses responders to respond to Matrix events, and comes with some sample
  responders as submodules of `Igor.Responder`.  For information on writing your
  own responder, see the documentation for `Igor.Responder`.

  ## Bot functions

  This module provides some functions that can be used in writing bots, such as
  sending messages and reactions.

  """

  use GenServer
  require Logger

  @doc """
  The handle for a bot.
  """
  @type t :: %__MODULE__{
          client: Polyjuice.Client.t(),
          bot_name: String.t(),
          command_prefix: Regex.t(),
          responders: [],
          storage: Polyjuice.Client.Storage.t(),
          opts: map
        }

  defstruct [
    :client,
    :storage,
    bot_name: "igor",
    command_prefix: ~r/^(?:igor:? +|!)/,
    responders: [],
    opts: %{}
  ]

  defmodule State do
    @moduledoc false
    @enforce_keys [:bot, :queue_supervisor]
    defstruct [
      :bot,
      :queue_supervisor,
      client_pid: nil,
      queues: %{}
    ]
  end

  @doc """
  Start the bot process.
  """
  @spec start_link(opts :: Keyword.t()) :: {:ok, pid}
  def start_link(opts) when is_list(opts) do
    GenServer.start_link(__MODULE__, opts)
  end

  @doc """
  Send a message to a room.

  The `message` can be a string, a tuple (where the first item is the message
  in plain text, and the second item is the message in HTML), an
  `Igor.Message`, or a map (giving the full message contents).

  The `room` can either be a room ID as a string, or the `Igor.Message` that
  the message is sent in response to.

  """
  @spec send(
          message :: Igor.Message.t() | Polyjuice.Client.MsgBuilder.MsgData.t() | map,
          room :: String.t() | Igor.Message.t(),
          bot :: Igor.t()
        ) :: Any
  def send(message, %Igor.Message{room: room}, bot) do
    send(message, room, bot)
  end

  def send(%Igor.Message{content: content}, room, %__MODULE__{client: client})
      when is_map(content) and is_binary(room) do
    Polyjuice.Client.Room.send_message(client, room, content)
  end

  def send(message, room, %__MODULE__{client: client}) when is_binary(room) do
    cond do
      Polyjuice.Client.MsgBuilder.MsgData.impl_for(message) != nil ->
        Polyjuice.Client.Room.send_message(
          client,
          room,
          Polyjuice.Client.MsgBuilder.to_message(message, "m.notice")
        )

      is_map(message) and not Map.has_key?(message, :__struct__) ->
        Polyjuice.Client.Room.send_message(client, room, message)

      true ->
        raise ArgumentError, message: "invalid argument msg"
    end
  end

  @doc false
  # Reply to a message
  def reply(_msg, _orig_msg, _bot) do
  end

  @doc """
  React to a message.
  """
  def react(reaction, %Igor.Message{room: room, event_id: event_id}, bot) do
    Polyjuice.Client.Room.send_event(
      bot.client,
      room,
      "m.reaction",
      %{
        "m.relates_to" => %{
          "rel_type" => "m.annotation",
          "event_id" => event_id,
          "key" => reaction
        }
      }
    )
  end

  @doc false
  # Redact an event.
  def redact(_orig_msg, _bot) do
  end

  @doc false
  # Try to parse a message to extract the text and command syntax (if applicable).
  def _parse_msg(
        %Igor.Message{
          content: %{
            "msgtype" => "m.text",
            "format" => "org.matrix.custom.html",
            "formatted_body" => formatted_body
          }
        } = msg,
        bot
      ) do
    text = formatted_body |> html_to_text |> String.trim()
    command = parse_text_to_command(text, bot)
    %Igor.Message{msg | text: text, parsed_command: command}
  end

  def _parse_msg(
        %Igor.Message{
          content: %{"msgtype" => "m.text", "body" => body}
        } = msg,
        bot
      ) do
    text = String.trim(body)
    command = parse_text_to_command(text, bot)
    %Igor.Message{msg | text: text, parsed_command: command}
  end

  @doc ~S"""
  Convert Matrix HTML to an approximate text rendering that should be more
  useful for parsing.  This will not necessarily be a useful text rendering for
  displaying to a user; it is only meant to be used for parsing.

  ## Examples

      iex> Igor.html_to_text("<mx-reply>Someone else's message</mx-reply>foo<img alt=\"bar\"><b>baz</b>")
      "foobarbaz"

      iex> Igor.html_to_text("Yes <a href=\"https://matrix.to/#/%40hubert:uhoreg.ca\">master</a>")
      "Yes  @hubert:uhoreg.ca "
  """
  def html_to_text(html) do
    html
    |> Html5ever.parse()
    |> (fn {:ok, items} -> dom_to_text(items) end).()
    |> IO.iodata_to_binary()
  end

  @doc false
  defp dom_to_text(items) when is_list(items) do
    Enum.map(items, &dom_to_text/1)
  end

  defp dom_to_text({name, attrs, children}) when is_list(attrs) and is_list(children) do
    cond do
      # inline elements that don't affect text flow
      String.match?(name, ~r/^(?:font|i|b|u|del|em|code|s(?:pan|tr(?:ong|ike)|u[bp]))$/i) ->
        dom_to_text(children)

      # matrix.to links get replaced by the ID that they refer to
      name === "a" ->
        try do
          {"href", href} = Enum.find(attrs, fn {name, _} -> name === "href" end)
          %URI{host: "matrix.to", path: "/", fragment: "/" <> mxid} = URI.parse(href)
          [" ", URI.decode_www_form(mxid), " "]
        catch
          _ -> dom_to_text(children)
        end

      # block elements -- newlines before and after
      String.match?(name, ~r/^(?:par|div|h[1-6]|blockquote|li|pre|ul|ol|caption|table)$/i) ->
        ["\n\n", dom_to_text(children), "\n\n"]

      # images get replaced with their alt tag or whitespace
      name === "img" ->
        case Enum.find(attrs, fn {name, _} -> name === "alt" end) do
          {"alt", alt} -> alt
          _ -> ?\s
        end

      String.match?(name, ~r/^(?:[bt]r)$/) ->
        ?\n

      name === "td" ->
        [dom_to_text(children), ?\t]

      name === "hr" ->
        "\n\n"

      # drop replied-to text
      name === "mx-reply" ->
        []

      # ignore any other tags
      true ->
        dom_to_text(children)
    end
  end

  defp dom_to_text(text) when is_binary(text), do: text

  @doc """
  Parse text into a command list if possible.

  ## Examples

      iex> Igor.parse_text_to_command("igor: foo bar baz", %Igor{})
      ["foo", "bar", "baz"]

      iex> Igor.parse_text_to_command("!foo bar baz", %Igor{})
      ["foo", "bar", "baz"]
  """
  def parse_text_to_command(text, bot) do
    case Regex.run(bot.command_prefix, text, return: :index) do
      [{0, len} | _] ->
        rest = binary_part(text, len, byte_size(text) - len)
        # FIXME: treat strings enclosed in '"' as literals
        String.split(rest)

      _ ->
        nil
    end
  end

  def escape_html(str) when is_binary(str) do
    map = %{
      "&" => "&amp;",
      "<" => "&lt;",
      ">" => "&gt;",
      "\"" => "&quot;"
    }

    Regex.replace(~r/[&<>"]/, str, &map[&1])
  end

  @doc ~S"""
  Strip out the replied-to message from a message reply in Matrix HTML format.
  Also sanitizes the HTML.

  ## Examples

      iex> Igor.strip_reply_html("<mx-reply>Someone else's message </mx-reply>foo<img alt=\"bar\"><b>baz</B><unclosed-tag>bla")
      "foo<img alt=\"bar\"><b>baz</b><unclosed-tag>bla</unclosed-tag>"
  """
  def strip_reply_html(html) do
    html
    |> Html5ever.parse()
    |> (fn {:ok, items} -> drop_reply(items) end).()
    |> IO.iodata_to_binary()
  end

  defp drop_reply(items) when is_list(items) do
    Enum.map(items, &drop_reply/1)
  end

  defp drop_reply({name, attrs, children}) when is_list(attrs) and is_list(children) do
    cond do
      name === "mx-reply" ->
        []

      # Html5ever adds <html>, <head>, and <body> tags, but we don't care about
      # those in Matrix messages, so ignore
      String.match?(name, ~r/^(?:h(?:tml|ead)|body)$/i) ->
        drop_reply(children)

      true ->
        [
          ?<,
          name,
          Enum.map(
            attrs,
            fn {a_name, value} -> [?\s, escape_html(a_name), "=\"", escape_html(value), ?"] end
          ),
          ?>,
          drop_reply(children),
          if(String.match?(name, ~r/[bh]r|img/), do: [], else: ["</", name, ?>])
        ]
    end
  end

  defp drop_reply(text) when is_binary(text), do: text

  @doc false
  # mainly for unit tests.  Anything else shouldn't have a reason to use it.
  def get_client(pid) do
    GenServer.call(pid, :get_client)
  end

  @doc false
  @spec init(opts :: Keyword.t()) :: tuple
  @impl true
  def init(opts) when is_list(opts) do
    opts = Map.new(opts)

    storage =
      case Map.get(opts, :storage, {Igor.Storage, :ets, []}) do
        {mod, func, args} when is_atom(mod) and is_atom(func) and is_list(args) ->
          apply(mod, func, args)

        {func, args} when is_function(func) and is_list(args) ->
          apply(func, args)

        x ->
          x
      end

    alias Polyjuice.Client.Filter

    filter =
      Filter.include_state_types([])
      |> Filter.include_presence_types([])
      |> Filter.include_ephemeral_types([])

    {client_pid, client} =
      case opts do
        %{start_client: start_client} ->
          {:ok, pid, client} = start_client.(self(), storage, filter)
          # FIXME: monitor client
          {pid, client}

        %{make_client: make_client} ->
          {nil, make_client.(self(), storage, filter)}

        %{client: client} ->
          {nil, client}

        _ ->
          {:ok, pid, client} =
            Polyjuice.Client.start_link_and_get_client(
              opts.homeserver_url,
              [
                {:access_token, Map.get(opts, :access_token)},
                {:user_id, Map.get(opts, :mxid)},
                {:handler, self()},
                {:storage, storage},
                {:sync_filter, filter} | Map.get(opts, :client_opts, [])
              ]
            )

          # FIXME: monitor client
          {pid, client}
      end

    user_id =
      case Map.get(opts, :mxid) do
        nil -> Polyjuice.Client.API.get_user_and_device(client) |> elem(0)
        user_id -> user_id
      end

    name_regexp_components =
      [
        Map.get(opts, :bot_name, "igor"),
        user_id
        | Map.get(opts, :aka, [])
      ]
      |> Enum.map(&Regex.escape/1)
      |> Enum.join("|")

    name_regexp = "(?:#{name_regexp_components}) *:? +"

    prefix_regexp =
      Map.get(opts, :command_prefixes, [])
      |> Enum.map(&Regex.escape/1)

    prefix_components =
      [name_regexp | prefix_regexp]
      |> Enum.join("|")

    {:ok, command_prefix} = Regex.compile("^(?:#{prefix_components})")

    responders =
      Map.get(opts, :responders, [])
      |> Enum.map(fn
        {mod, func, args} when is_atom(mod) and is_atom(func) and is_list(args) ->
          apply(mod, func, args)

        {func, args} when is_function(func) and is_list(args) ->
          apply(func, args)

        x ->
          x
      end)

    {:ok, queue_supervisor} = Task.Supervisor.start_link()

    {:ok,
     %State{
       bot: %__MODULE__{
         client: client,
         bot_name: Map.fetch!(opts, :bot_name),
         command_prefix: command_prefix,
         responders: responders,
         storage: storage,
         opts: opts
       },
       client_pid: client_pid,
       queue_supervisor: queue_supervisor
     }}
  end

  defp respond_to_message(
         %{room: room, event_id: event_id} = msg,
         %{client: client, responders: responders} = bot
       ) do
    if is_list(msg.parsed_command) and not Enum.empty?(msg.parsed_command) do
      Polyjuice.Client.Room.update_read_receipt(client, room, event_id)
    end

    Enum.each(responders, fn responder ->
      Igor.Responder.Proto.respond(responder, msg, bot)
    end)
  end

  defp add_to_queue(%Igor.Message{room: room} = msg, %State{bot: bot, queues: queues} = state) do
    if not Map.has_key?(queues, room) do
      # we don't have anything processing messages for this room yet, so start a new task
      Task.Supervisor.start_child(
        state.queue_supervisor,
        Igor,
        :process_message_queue,
        [room, bot, self()],
        restart: :transient
      )
    end

    # add the message to the room's queue
    Map.get(queues, room, :queue.new())
    |> (&:queue.in(msg, &1)).()
    |> (&Map.put(queues, room, &1)).()
    |> (&Map.put(state, :queues, &1)).()
  end

  @doc false
  # process messages from a room's queue
  def process_message_queue(room, bot, pid) do
    case GenServer.call(pid, {:get_next_event, room}) do
      nil ->
        nil

      %{} = msg ->
        respond_to_message(msg, bot)

        process_message_queue(room, bot, pid)
    end
  end

  @doc false
  @impl GenServer
  def handle_info({:polyjuice_client, :message, {room, message}}, %State{bot: bot} = state) do
    new_state =
      case message do
        %{
          "content" => %{"msgtype" => "m.text"} = content,
          "type" => "m.room.message",
          "sender" => sender,
          "event_id" => event_id
        } ->
          %Igor.Message{
            sender: sender,
            room: room,
            event_id: event_id,
            type: "m.room.message",
            content: content
          }
          |> Igor._parse_msg(bot)
          |> add_to_queue(state)

        _ ->
          state
      end

    {:noreply, new_state}
  end

  @impl GenServer
  def handle_info(
        {:polyjuice_client, :invite, {room, inviter, _invite_state}},
        %{bot: %{opts: opts, client: client}} = state
      ) do
    accept =
      case Map.get(opts, :accept_invites, true) do
        true ->
          true

        allowed when is_list(allowed) ->
          Enum.member?(allowed, inviter)

        _ ->
          false
      end

    if accept do
      Logger.debug("invited to #{room} by #{inviter} -- joining")
      Polyjuice.Client.Room.join(client, room)
    else
      Logger.debug("invited to #{room} by #{inviter} -- rejecting")
      Polyjuice.Client.Room.leave(client, room)
      Polyjuice.Client.Room.forget(client, room)
    end

    {:noreply, state}
  end

  @impl GenServer
  def handle_info(_, state) do
    {:noreply, state}
  end

  @impl GenServer
  def handle_call(:get_client, _, %{bot: %{client: client}} = state) do
    {:reply, client, state}
  end

  @impl GenServer
  def handle_call({:get_next_event, room_id}, _, %{queues: queues} = state) do
    case Map.get(queues, room_id) do
      nil ->
        {:reply, nil, state}

      room_queue ->
        {val, new_queue} = :queue.out(room_queue)

        reply =
          case val do
            {:value, msg} -> msg
            _ -> nil
          end

        new_queues =
          if :queue.is_empty(new_queue) do
            Map.delete(queues, room_id)
          else
            Map.put(queues, room_id, new_queue)
          end

        {:reply, reply, %{state | queues: new_queues}}
    end
  end

  @doc false
  @impl GenServer
  def terminate(_, state) do
    Polyjuice.Client.Storage.close(state.bot.storage)

    if state.queue_supervisor do
      DynamicSupervisor.stop(state.queue_supervisor)
    end

    if state.client_pid do
      Process.unlink(state.client_pid)
      Process.exit(state.client_pid, :normal)
    end
  end
end

defmodule Igor.Message do
  @moduledoc """
  Received Matrix message.

  This module represents a Matrix message that was received by Igor.

  The `text` field is a plain-text version of the message.  `parsed_command` is
  a result of attempting to parse `text` as a command.  If it cannot be parsed
  as a command, it will be `nil`.

  """

  @type t :: %__MODULE__{
          sender: String.t(),
          room: String.t(),
          event_id: String.t(),
          type: String.t(),
          content: map,
          text: String.t(),
          parsed_command: list
        }

  defstruct [
    :sender,
    :room,
    :event_id,
    type: "m.room.message",
    content: %{},
    text: nil,
    parsed_command: []
  ]
end

# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Igor.Responder do
  @moduledoc """
  Igor responders.


  See `Igor.Responder.Proto` for details on how to how responders are written.
  `Igor.Responder.Helper` and `Igor.Responder.ModuleHelper` define macros to
  help with writing responders.
  """

  defprotocol Proto do
    @moduledoc """
    Responder protocol.

    See also `Igor.Responder.Helper` and `Igor.Responder.ModuleHelper`, which
    define macros to help with writing responders.

    Any module can be used as a responder if it implements `respond(msg, bot)`
    """

    @doc """
    Respond to a message.

    This function will be called whenever a message is received, and the
    responder can respond to the message, for example by calling `Igor.send/3`.
    `responder` is the responder itself, `msg` is the message to respond to,
    and `bot` is a handle for the bot, which can be used when calling the
    `Igor.*` functions.

    """
    @spec respond(responder :: __MODULE__.t(), msg :: Igor.Message.t(), bot :: Igor.t()) :: any
    def respond(responder, msg, bot)
  end

  defimpl Igor.Responder.Proto, for: Atom do
    # assume that an atom is a module name, and call the `respond` method
    def respond(module, msg, bot) do
      apply(module, :respond, [msg, bot])
    end
  end

  defmodule Helper do
    @moduledoc """
    Helper for writing responders.

    This is similar to `Igor.Responder.ModuleHelper`, but allows for the
    responder to be bound to some data.

    An example of a responder using this is `Igor.Responder.Uptime`.

    """

    @doc false
    defmacro __using__(_opts) do
      quote do
        @behaviour Igor.Responder.Helper
        import Igor.Responder.Helper
        Module.register_attribute(__MODULE__, :responder_listeners, accumulate: true)
        Module.register_attribute(__MODULE__, :help, accumulate: true)
        @before_compile Igor.Responder.Helper
      end
    end

    @doc false
    defmacro command(responder, pattern, msg, bot, do: block) do
      quote do
        @doc false
        def respond(
              unquote(responder),
              %Igor.Message{parsed_command: unquote(pattern)} = unquote(msg),
              unquote(bot)
            ),
            do: unquote(block)
      end
    end

    @doc false
    defmacro listen(responder, regexp, matches, msg, bot, do: block) do
      function_name = String.to_atom("igor_listener #{Macro.to_string(regexp)}")

      quote do
        @responder_listeners {unquote(regexp), unquote(function_name)}
        @doc false
        def unquote(function_name)(
              unquote(responder),
              unquote(matches),
              %Igor.Message{} = unquote(msg),
              %Igor{} = unquote(bot)
            ),
            do: unquote(block)
      end
    end

    @doc false
    defmacro __before_compile__(env) do
      quote do
        @doc false
        def respond(responder, %Igor.Message{} = msg, %Igor{} = bot) do
          if is_list(@responder_listeners) and not Enum.empty?(@responder_listeners) do
            Enum.each(@responder_listeners, fn {re, f} ->
              matches = Regex.scan(re, msg.text)

              if not Enum.empty?(matches),
                do: apply(__MODULE__, f, [responder, matches, msg, bot])
            end)
          end
        end

        defoverridable respond: 3

        @doc false
        def help(responder, cmdprefix) when is_binary(cmdprefix) do
          Enum.map(
            @help,
            &Igor.Responder.Help.help_summary(cmdprefix, &1)
          )
        end

        @doc false
        def help(responder, cmdprefix, [cmd]) when is_binary(cmdprefix) and is_binary(cmd) do
          Enum.find_value(
            @help,
            fn
              %{command: ^cmd} = help ->
                [
                  Igor.Responder.Help.help_summary(cmdprefix, help),
                  case Map.get(help, :long) do
                    nil -> ""
                    long -> ["\n\n", long]
                  end
                ]

              %{command: [^cmd]} = help ->
                [
                  Igor.Responder.Help.help_summary(cmdprefix, help),
                  case Map.get(help, :long) do
                    nil -> ""
                    long -> ["\n\n", long]
                  end
                ]

              _ ->
                nil
            end
          )
        end

        def help(responder, cmdprefix, cmd) when is_binary(cmdprefix) and is_list(cmd) do
          Enum.find_value(
            @help,
            fn
              %{command: ^cmd} = help ->
                [
                  Igor.Responder.Help.help_summary(cmdprefix, help),
                  case Map.get(help, :long) do
                    nil -> ""
                    long -> ["\n\n", long]
                  end
                ]

              _ ->
                nil
            end
          )
        end

        defoverridable help: 2, help: 3

        defimpl Igor.Responder.Proto do
          def respond(responder, msg, bot) do
            unquote(env.module).respond(responder, msg, bot)
          end
        end

        defimpl Igor.Responder.Help.Proto do
          def help(responder, botname) do
            unquote(env.module).help(responder, botname)
          end

          def help(responder, botname, command) do
            unquote(env.module).help(responder, botname, command)
          end
        end
      end
    end

    @doc false
    @callback respond(
                responder :: Igor.Responder.Proto.t(),
                msg :: Igor.Message.t(),
                bot :: Igor.t()
              ) :: any

    @doc false
    @callback help(responder :: Igor.Responder.Proto.t(), cmdprefix :: String.t()) :: [String.t()]

    @doc false
    @callback help(
                responder :: Igor.Responder.Proto.t(),
                cmdprefix :: String.t(),
                cmd :: String.t()
              ) :: String.t() | nil
  end

  defmodule ModuleHelper do
    @moduledoc """
    Helper for writing responder modules.

    Responses can be defined by using the `command` or `listen` macros.  If those
    macros are not sufficient, responses can be defined by defining a `respond/2`
    function, where the first argument is an `Igor.Message` and the second
    argument is an `Igor`.

    Responses can be documented using the `@help` attribute.  These will show up
    when a user uses the `help` command, if the `Igor.Responder.Help` module is
    used.  The value should be a map that can contain the following keys:

    - `command :: String.t() | [String.t()]` (required): the command that is
      implemented.  If it is a sub-command, then this should be a list.
      Otherwise, it can be a string or a 1-element list.  For example, a GitLab
      responder could implement a `gitlab create` command, in which case, this
      would be `["gitlab", "create"]`.
    - `args :: Polyjuice.Client.MsgBuilder.MsgData.t()` (optional): the
      arguments that the command takes.
    - `short :: Polyjuice.Client.MsgBuilder.MsgData.t()` (optional): a brief
      description of what the command does.  This will be shown when displaying
      the list of commands that the bot understands.
    - `long :: Polyjuice.Client.MsgBuilder.MsgData.t()` (optional): a detailed
      description of what the command does.  This will be shown when the user
      requests help for the command.

    ## Example

        defmodule Ping do
          use Igor.Responder

          @help %{command: "ping", args: "[response]", short: "respond with a pong"}
          command ["ping"], msg, bot do
            "pong" |> Igor.send(msg, bot)
          end
        end

    More complicated responders (e.g. that require state) can be created by
    writing a module that implements the `Igor.Responder.Proto` protocol.
    """

    @doc false
    defmacro __using__(_opts) do
      quote do
        @behaviour Igor.Responder.ModuleHelper
        import Igor.Responder.ModuleHelper
        Module.register_attribute(__MODULE__, :responder_listeners, accumulate: true)
        Module.register_attribute(__MODULE__, :help, accumulate: true)
        @before_compile Igor.Responder.ModuleHelper
      end
    end

    @doc """
    Defines a command that the bot understands.

    A command is represented as a list of strings, and can be pattern matched.

    ## Examples

    Define a command that adds numbers together.

        command(["add" | items], msg, bot) do
          Enum.map(items, &Float.parse)
            |> Enum.reduce(fn x, acc -> x + acc end)
            |> to_string
            |> Igor.send(msg, bot)
        end

    A command can have multiple syntaxes.

        command(["roll"], msg, bot) do
          # if no argument is given, roll a d6
          :rand.uniform(6) |> to_string |> Igor.send(msg, bot)
        end
        command(["roll" | dice], msg, bot) do
          # if arguments are given, roll the specified dice
          dice |> Enum.flat_map(fn spec ->
              {times, max} = parse_spec(spec) # parse_spec is left as an exercise for the reader
              Enum.map 1..times, fn _ -> :rand.uniform(max) |> to_string end
            end)
            |> Enum.join(" ")
            |> Igor.send(msg, bot)
        end
    """
    defmacro command(pattern, msg, bot, do: block) do
      quote do
        @doc false
        def respond(%Igor.Message{parsed_command: unquote(pattern)} = unquote(msg), unquote(bot)),
          do: unquote(block)
      end
    end

    @doc ~S"""
    Respond to messages that match a regular expression.

    ## Examples

    Respond to messages that contain `#(number)` with a link to the issue tracker.

        listen ~r/#(\d+)/, matches, msg, bot do
          Enum.map(
            matches,
            fn [_, issuenum] ->
              ["https://gitlab.com/uhoreg/igor/issues/", issuenum]
            end
          )
        end
        |> intersperse("\n")
        |> Igor.send(msg, bot)
    """
    defmacro listen(regexp, matches, msg, bot, do: block) do
      function_name = String.to_atom("igor_listener #{Macro.to_string(regexp)}")

      quote do
        @responder_listeners {unquote(regexp), unquote(function_name)}
        @doc false
        def unquote(function_name)(
              unquote(matches),
              %Igor.Message{} = unquote(msg),
              %Igor{} = unquote(bot)
            ),
            do: unquote(block)
      end
    end

    @doc false
    defmacro __before_compile__(_env) do
      quote do
        @doc false
        def respond(%Igor.Message{} = msg, %Igor{} = bot) do
          if is_list(@responder_listeners) and not Enum.empty?(@responder_listeners) do
            Enum.each(@responder_listeners, fn {re, f} ->
              matches = Regex.scan(re, msg.text)
              if not Enum.empty?(matches), do: apply(__MODULE__, f, [matches, msg, bot])
            end)
          end
        end

        defoverridable respond: 2

        @doc false
        def help(cmdprefix) when is_binary(cmdprefix) do
          Enum.map(
            @help,
            &Igor.Responder.Help.help_summary(cmdprefix, &1)
          )
        end

        @doc false
        def help(cmdprefix, [cmd]) when is_binary(cmdprefix) and is_binary(cmd) do
          Enum.find_value(
            @help,
            fn
              %{command: ^cmd} = help ->
                [
                  Igor.Responder.Help.help_summary(cmdprefix, help),
                  case Map.get(help, :long) do
                    nil -> ""
                    long -> ["\n\n", long]
                  end
                ]

              %{command: [^cmd]} = help ->
                [
                  Igor.Responder.Help.help_summary(cmdprefix, help),
                  case Map.get(help, :long) do
                    nil -> ""
                    long -> ["\n\n", long]
                  end
                ]

              _ ->
                nil
            end
          )
        end

        def help(cmdprefix, cmd) when is_binary(cmdprefix) and is_list(cmd) do
          Enum.find_value(
            @help,
            fn
              %{command: ^cmd} = help ->
                [
                  Igor.Responder.Help.help_summary(cmdprefix, help),
                  case Map.get(help, :long) do
                    nil -> ""
                    long -> ["\n\n", long]
                  end
                ]

              _ ->
                nil
            end
          )
        end

        defoverridable help: 1, help: 2
      end
    end

    @doc """
    Invoked to respond to a message.
    """
    @callback respond(msg :: Igor.Message.t(), bot :: Igor.t()) :: any

    @doc """
    Invoked to get the help messages.
    """
    @callback help(cmdprefix :: String.t()) :: [String.t()]

    @doc """
    Invoked to get the help message for a certain command.
    """
    @callback help(cmdprefix :: String.t(), cmd :: String.t()) :: String.t() | nil
  end

  defmodule GenServerHelper do
    @moduledoc """
    Helper for writing responders based on `GenServer`.

    This is similar to `Igor.Responder.ModuleHelper`, but allows for the
    responder to be in its own process.

    To use this helper, the module must define a struct that has a `:pid`
    field, which must store the process ID of the `GenServer`.  It is
    recommended that it also provide a function that will start the server and
    return a struct.

    The module must also define an `init/1` function as required by `GenServer`.

    This helper provides `command` and `listen` macros, like
    `Igor.Responder.ModuleHelper`, but with an extra argument at the end, which
    is the current state of the `GenServer`.  The return value must the same as
    what would be returned by a `handle_cast/2` method for a `GenServer`.

    An example of a responder using this is `Igor.Responder.Slap`.

    """

    @doc false
    defmacro __using__(_opts) do
      quote do
        @behaviour Igor.Responder.GenServerHelper
        import Igor.Responder.GenServerHelper
        use GenServer
        Module.register_attribute(__MODULE__, :responder_listeners, accumulate: true)
        Module.register_attribute(__MODULE__, :help, accumulate: true)
        @before_compile Igor.Responder.GenServerHelper
      end
    end

    defmacro command(pattern, msg, bot, state, do: block) do
      quote do
        @doc false
        def handle_cast(
              {:igor_respond, %Igor.Message{parsed_command: unquote(pattern)} = unquote(msg),
               unquote(bot)},
              unquote(state)
            ),
            do: unquote(block)
      end
    end

    defmacro listen(regexp, matches, msg, bot, do: block) do
      function_name = String.to_atom("igor_listener #{Macro.to_string(regexp)}")

      quote do
        @responder_listeners {unquote(regexp), unquote(function_name)}
        @doc false
        def unquote(function_name)(
              unquote(matches),
              %Igor.Message{} = unquote(msg),
              %Igor{} = unquote(bot)
            ),
            do: unquote(block)
      end
    end

    @doc false
    defmacro __before_compile__(env) do
      quote do
        @doc false
        def handle_cast({:igor_respond, %Igor.Message{} = msg, %Igor{} = bot}, state) do
          if is_list(@responder_listeners) and not Enum.empty?(@responder_listeners) do
            Enum.find_value(@responder_listeners, {:noreply, state}, fn {re, f} ->
              matches = Regex.scan(re, msg.text)

              if not Enum.empty?(matches),
                do: apply(__MODULE__, f, [matches, msg, bot, state])
            end)
          else
            {:noreply, state}
          end
        end

        @doc false
        def help(responder, cmdprefix) when is_binary(cmdprefix) do
          Enum.map(
            @help,
            &Igor.Responder.Help.help_summary(cmdprefix, &1)
          )
        end

        @doc false
        def help(responder, cmdprefix, [cmd]) when is_binary(cmdprefix) and is_binary(cmd) do
          Enum.find_value(
            @help,
            fn
              %{command: ^cmd} = help ->
                [
                  Igor.Responder.Help.help_summary(cmdprefix, help),
                  case Map.get(help, :long) do
                    nil -> ""
                    long -> ["\n\n", long]
                  end
                ]

              %{command: [^cmd]} = help ->
                [
                  Igor.Responder.Help.help_summary(cmdprefix, help),
                  case Map.get(help, :long) do
                    nil -> ""
                    long -> ["\n\n", long]
                  end
                ]

              _ ->
                nil
            end
          )
        end

        def help(responder, cmdprefix, cmd) when is_binary(cmdprefix) and is_list(cmd) do
          Enum.find_value(
            @help,
            fn
              %{command: ^cmd} = help ->
                [
                  Igor.Responder.Help.help_summary(cmdprefix, help),
                  case Map.get(help, :long) do
                    nil -> ""
                    long -> ["\n\n", long]
                  end
                ]

              _ ->
                nil
            end
          )
        end

        defoverridable help: 2, help: 3

        defimpl Igor.Responder.Proto do
          def respond(%{pid: pid}, msg, bot) do
            GenServer.cast(pid, {:igor_respond, msg, bot})
          end
        end

        defimpl Igor.Responder.Help.Proto do
          def help(responder, botname) do
            unquote(env.module).help(responder, botname)
          end

          def help(responder, botname, command) do
            unquote(env.module).help(responder, botname, command)
          end
        end
      end
    end

    @doc false
    @callback help(responder :: Igor.Responder.Proto.t(), cmdprefix :: String.t()) :: [String.t()]

    @doc false
    @callback help(
                responder :: Igor.Responder.Proto.t(),
                cmdprefix :: String.t(),
                cmd :: String.t()
              ) :: String.t() | nil
  end
end

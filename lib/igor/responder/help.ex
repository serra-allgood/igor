# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Igor.Responder.Help do
  @moduledoc """
  Displays help for commands.

  Responders should implement the `Igor.Responder.Help.Proto` protocol, so that
  this responder can display help for it.

  Responders that use `Igor.Responder.Helper` or `Igor.Responder.ModuleHelper`
  will do this automatically, and can use the `@help` module attribute to set
  the help information.  See the documentation for `Igor.Responder.Helper` and
  `Igor.Responder.ModuleHelper` for more information.

  """

  use Igor.Responder.ModuleHelper

  defprotocol Proto do
    @doc """
    Get the general help for the responder.

    A prefix will be passed; commands should be prefixed by the given prefix in
    the help text.  It should return an array of items that implement the
    `Polyjuice.Client.MsgBuilder.MsgData` protocol; each item will be a
    separate line in the output for the `help` command.

    """
    @spec help(responder :: __MODULE__.t(), cmdprefix :: String.t()) :: [
            Polyjuice.Client.MsgBuilder.MsgData.t()
          ]
    def help(responder, cmdprefix)

    @doc """
    Get the help text for a specific command.

    A prefix will be passed; the command should be prefixed by the given prefix
    in the help text.  If the responder implements the given command, it should
    return detailed help for the command.  Otherwise, it should return `nil`.
    The command may have multiple components, and so is given as a list.

    """
    @spec help(responder :: __MODULE__.t(), cmdprefix :: String.t(), command :: list()) ::
            Polyjuice.Client.MsgBuilder.MsgData.t() | nil
    def help(responder, cmdprefix, command)
  end

  defimpl Igor.Responder.Help.Proto, for: Atom do
    def help(module, botname) do
      apply(module, :help, [botname])
    end

    def help(module, botname, command) do
      apply(module, :help, [botname, command])
    end
  end

  @help %{
    command: "help",
    args: "[command...]",
    short: "display help",
    long: [
      "If ",
      {"`command`", "<code>command</code>"},
      " is given, then display help for the given command.  Otherwise, display a list of commands that the bot understands."
    ]
  }
  command ["help"], orig_msg, %{responders: responders} = bot do
    cmdprefix = Map.get(bot.opts, :help_command_prefix, bot.bot_name <> " ")

    responders
    |> Enum.filter(&(Igor.Responder.Help.Proto.impl_for(&1) != nil))
    |> Enum.map(&Igor.Responder.Help.Proto.help(&1, cmdprefix))
    |> Enum.concat()
    |> Enum.intersperse("\n")
    |> Igor.send(orig_msg, bot)
  end

  command ["help" | command], orig_msg, %{responders: responders} = bot do
    cmdprefix = Map.get(bot.opts, :help_command_prefix, bot.bot_name <> " ")

    (responders
     |> Enum.filter(&(Igor.Responder.Help.Proto.impl_for(&1) != nil))
     |> Enum.find_value(&Igor.Responder.Help.Proto.help(&1, cmdprefix, command)) ||
       "Unknown command.")
    |> Igor.send(orig_msg, bot)
  end

  @spec help_summary(cmdprefix :: String.t(), help :: map) ::
          Polyjuice.Client.MsgBuilder.MsgData.t()
  def help_summary(cmdprefix, %{command: cmd} = help) do
    [
      cmdprefix,
      if(is_list(cmd), do: Enum.intersperse(cmd, " "), else: cmd),
      case Map.get(help, :args) do
        nil -> ""
        args -> [" ", args]
      end,
      case Map.get(help, :short) do
        nil -> ""
        short -> [" - ", short]
      end
    ]
  end

  def help_summary(_, _), do: ""
end

# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Igor.Responder.React do
  @moduledoc """
  Responds to messages with silly reactions.
  """

  use Igor.Responder.ModuleHelper

  listen ~r/gb/i, _matches, msg, bot do
    "🇬🇧" |> Igor.react(msg, bot)
  end

  listen ~r/\bcan?\b/i, _matches, msg, bot do
    "🇨🇦" |> Igor.react(msg, bot)
  end

  listen ~r/\b(?:b+r+a+i+n+|z+o+m+b+i+e+)s*\b/i, _matches, msg, bot do
    "🧠" |> Igor.react(msg, bot)
  end

  listen ~r/\barms?\b/i, _matches, msg, bot do
    "💪" |> Igor.react(msg, bot)
  end

  listen ~r/\blegs?\b/i, _matches, msg, bot do
    "🦵" |> Igor.react(msg, bot)
  end

  listen ~r/\bcookies?\b/i, _matches, msg, bot do
    "🍪" |> Igor.react(msg, bot)
  end
end

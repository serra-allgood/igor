# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Igor.Responder.MscLink do
  @moduledoc """
  Includes links to MSCs that are mentioned.
  """

  use Igor.Responder.ModuleHelper

  listen ~r/MSC(\d+)/i, matches, orig_msg, bot do
    Enum.map(
      matches,
      fn [_, mscnum] ->
        ["https://github.com/matrix-org/matrix-doc/issues/", mscnum]
      end
    )
    |> Enum.intersperse("\n")
    |> Igor.send(orig_msg, bot)
  end
end

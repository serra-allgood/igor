# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Igor.Responder.Slap do
  @moduledoc """
  Slap users.

  Tell the bot to slap users with a dynamic set of weapons.

  A port of https://code.matthewwild.co.uk/riddim/file/tip/plugins/slap.lua

  """

  use Igor.Responder.GenServerHelper

  @enforce_keys [:pid]
  defstruct [:pid]

  def create(weapons \\ ["a large trout"]) do
    {:ok, pid} = GenServer.start(Igor.Responder.Slap, MapSet.new(weapons))
    %Igor.Responder.Slap{pid: pid}
  end

  @impl true
  def init(weapons) do
    {:ok, weapons}
  end

  @help %{
    command: "slap",
    args: "[user]",
    short: "slap a user",
    long:
      "Slap a user with a weapon.  Weapons are managed with the 'pick up' and 'drop' commands."
  }
  command ["slap" | user], %Igor.Message{sender: sender} = orig_msg, bot, weapons do
    target =
      case user do
        [] ->
          Polyjuice.Client.MsgBuilder.mention(sender)

        [u] ->
          if Regex.match?(~r/^@.*:.*/, u) do
            Polyjuice.Client.MsgBuilder.mention(u)
          else
            u
          end

        _ ->
          Enum.intersperse(user, " ")
      end

    ["slaps ", target, " with ", Enum.random(weapons)]
    |> Polyjuice.Client.MsgBuilder.to_message("m.emote")
    |> Igor.send(orig_msg, bot)

    {:noreply, weapons}
  end

  @help %{
    command: ["pick", "up"],
    args: "<weapon>",
    short: "pick up a weapon to use for slapping"
  }
  command ["pick", "up"], orig_msg, bot, weapons do
    "Tell me what weapon to pick up" |> Igor.send(orig_msg, bot)
    {:noreply, weapons}
  end

  command ["pick", "up", weapon], orig_msg, bot, weapons do
    case String.downcase(weapon) do
      "excalibur" ->
        "Listen -- strange women lying in ponds distributing swords is no basis for a system of government.  Supreme executive power derives from a mandate from the masses, not from some farcical aquatic ceremony."
        |> Igor.send(orig_msg, bot)

        {:noreply, weapons}

      "paper" ->
        ~S("Reverse primary thrust, Marvin." That's what they say to me. "Open airlock number 3, Marvin." "Marvin, can you pick up that piece of paper?" Here I am, brain the size of a planet, and they ask me to pick up a piece of paper.)
        |> Igor.send(orig_msg, bot)

        {:noreply, weapons}

      _ ->
        ["picks up ", weapon]
        |> Polyjuice.Client.MsgBuilder.to_message("m.emote")
        |> Igor.send(orig_msg, bot)

        {:noreply, MapSet.put(weapons, weapon)}
    end
  end

  command ["pick", "up" | weapon_list], orig_msg, bot, weapons do
    weapon = Enum.join(weapon_list, " ")

    ["picks up ", weapon]
    |> Polyjuice.Client.MsgBuilder.to_message("m.emote")
    |> Igor.send(orig_msg, bot)

    {:noreply, MapSet.put(weapons, weapon)}
  end

  @help %{
    command: "drop",
    args: "<weapon>",
    short: "drop a weapon"
  }
  command ["drop"], orig_msg, bot, weapons do
    "Tell me what weapon to drop" |> Igor.send(orig_msg, bot)
    {:noreply, weapons}
  end

  command ["drop" | weapon_list], orig_msg, bot, weapons do
    weapon = Enum.join(weapon_list, " ")

    if MapSet.member?(weapons, weapon) do
      if MapSet.size(weapons) == 1 do
        "refuses to drop its last weapon"
        |> Polyjuice.Client.MsgBuilder.to_message("m.emote")
        |> Igor.send(orig_msg, bot)

        {:noreply, weapons}
      else
        ["drops ", weapon]
        |> Polyjuice.Client.MsgBuilder.to_message("m.emote")
        |> Igor.send(orig_msg, bot)

        {:noreply, MapSet.delete(weapons, weapon)}
      end
    else
      ["doesn't have ", weapon]
      |> Polyjuice.Client.MsgBuilder.to_message("m.emote")
      |> Igor.send(orig_msg, bot)

      {:noreply, weapons}
    end
  end
end

# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Igor.Responder.Timestamp do
  @moduledoc """
  Convert a time stamp to a date.
  """

  use Igor.Responder.ModuleHelper

  @help %{
    command: "timestamp",
    args: "<timestamp>",
    short: "convert a time stamp to a date"
  }
  command ["timestamp", timestamp], orig_msg, bot do
    timestamp
    |> String.to_integer()
    |> DateTime.from_unix!()
    |> DateTime.to_iso8601()
    |> Igor.send(orig_msg, bot)
  end

  @help %{
    command: "timestampms",
    args: "<timestamp>",
    short: "convert a time stamp (in milliseconds) to a date"
  }
  command ["timestampms", timestampms], orig_msg, bot do
    timestampms
    |> String.to_integer()
    |> DateTime.from_unix!(:millisecond)
    |> DateTime.to_iso8601()
    |> Igor.send(orig_msg, bot)
  end
end

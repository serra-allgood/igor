# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Igor.Responder.Uptime do
  @moduledoc """
  Report how long the bot has been running for.

  This is a simple example of how to write a responder that is more than just a
  simple module.
  """

  use Igor.Responder.Helper

  defstruct [:starttime]

  def create do
    %__MODULE__{starttime: :erlang.monotonic_time(:second)}
  end

  @help %{
    command: "uptime",
    short: "show how long the bot has been running"
  }
  command %{starttime: starttime}, ["uptime"], orig_msg, bot do
    uptime = :erlang.monotonic_time(:second) - starttime
    uptime_s = rem(uptime, 60)
    uptime_s_div = div(uptime, 60)
    uptime_m = rem(uptime_s_div, 60)
    uptime_m_div = div(uptime_s_div, 60)
    uptime_h = rem(uptime_m_div, 24)
    uptime_d = div(uptime_m_div, 24)

    [
      "I have been running for ",
      if(uptime_d != 0, do: [to_string(uptime_d), " days, "], else: []),
      :io_lib.format("~2..0B:~2..0B:~2..0B", [uptime_h, uptime_m, uptime_s])
    ]
    |> IO.iodata_to_binary()
    |> Igor.send(orig_msg, bot)
  end
end

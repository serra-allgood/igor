# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Igor.Responder.Ping do
  @moduledoc """
  Responds to pings with pongs.
  """

  use Igor.Responder.ModuleHelper

  @help %{
    command: "ping",
    args: "[response]",
    short: "respond with a pong",
    long:
      "Respond with a pong.  If the optional response is given, then it will be used as the pong."
  }
  command ["ping"], %Igor.Message{sender: sender} = orig_msg, bot do
    [Polyjuice.Client.MsgBuilder.mention(sender), ": pong"]
    |> Igor.send(orig_msg, bot)
  end

  command ["ping" | resp], orig_msg, bot do
    resp |> Enum.join(" ") |> Igor.send(orig_msg, bot)
  end
end

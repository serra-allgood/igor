# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Mix.Tasks.Igor.Run do
  @moduledoc """
  Runs Igor as a standalone bot.

  Igor should first be configured using a `config/config.exs` file, under a
  `config :igor, ...` statement.  See Igor's `README.md` or
  `config/config_sample.exs` files for more information.

  """
  @shortdoc "Runs Igor as a standalone bot"
  use Mix.Task

  @impl Mix.Task
  def run(args) do
    Mix.Task.run("app.start", args)
    {:ok, pid} = Igor.start_link(Application.get_all_env(:igor))
    ref = Process.monitor(pid)

    receive do
      {:DOWN, ^ref, :process, _object, reason} -> reason
    end
  end
end
